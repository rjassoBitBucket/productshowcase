package com.brighterbrain.productshowcase.fragments;

import java.util.ArrayList;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.brighterbrain.productshowcase.R;
import com.brighterbrain.productshowcase.utils.DBManager;

/**This fragment shows a list from the products on the data base
 * and launch the selected one 
 *  @author Rafael Jasso
 */
public class ShowProductSelectorFragment extends Fragment {

	private ListView showProductListView;
	private DBManager dbConection;
	private Cursor dbCursor;
	private ArrayList<String> titleList;
	private ArrayList<Integer> idList;
	private ProgressDialog ringProgressDialog;
	private String  itemValue;
	private ShowProductFragment showProductFragment;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		//Load GUI
		View showProductsSelectorView = inflater.inflate(R.layout.show_products_selector_fragment, container, false);
		
		//Variables for data base connection
		dbConection = new DBManager(getActivity());
		dbConection.openDatabase();
		dbCursor = dbConection.getRows();
		
		//Two List for store id and product's titles;
		titleList = new ArrayList<String>();
		idList = new ArrayList<Integer>();
		
		while(dbCursor.moveToNext()){
			idList.add(dbCursor.getInt(0));
			titleList.add(dbCursor.getString(1));
		}
		
		//Load the list to set the adapter and show data
		showProductListView = (ListView)showProductsSelectorView.findViewById(R.id.showProductListView);
		
		 // Defined Array values to show in ListView
        String[] values = new String[titleList.size()];
        values = titleList.toArray(new String[titleList.size()]);
        
        //If the listview is empty return to the menu and shows advice
        if(titleList.size()==0){
        	Toast.makeText(getActivity(), getString(R.string.there_s_no_products_to_display_please_insert_them_first), Toast.LENGTH_SHORT).show();
        	getActivity().getFragmentManager().popBackStack();
        }
        
        //Initialize adapter
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1, values);
        
        showProductListView.setAdapter(adapter);
        
        //Onclick event on items
        showProductListView.setOnItemClickListener(new OnItemClickListener() {
	        @Override
	        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		         itemValue    = (String) showProductListView.getItemAtPosition(position);
		         ringProgressDialog = ProgressDialog.show(getActivity(), "Please wait ...", "Loading products ...", true);
				 ringProgressDialog.setCancelable(false);
		         new showProductTask().execute();
	        }

        });    
		return showProductsSelectorView;
	}
	
	/**
	 * this method helps you to get the correct index 
	 * of the products
	 */
	private int getProductIndex(String title){
		int index = 0;
		for(int i = 0;i<titleList.size();i++){
			if(titleList.get(i).equals(title)){
				index = idList.get(i);
				break;
			}
		}
		return index;
	}
	
	//Realize memory
	@Override
	public void onDestroy() {
		super.onDestroy();
		if(!(dbCursor==null)&&(!dbCursor.isClosed()))
			dbCursor.close();
	}
		
	//Async task to show progress bar
	private class showProductTask extends AsyncTask<Void, Integer, Long> {

		@Override
		protected Long doInBackground(Void... params) {			
			 Bundle args = new Bundle();
		     args.putInt("productIndex", getProductIndex(itemValue));
		     showProductFragment = new ShowProductFragment();
		     showProductFragment.setArguments(args);
		     
			return null;
		}
		 protected void onProgressUpdate(Integer... progress) {
	     }
		 
		protected void onPostExecute(Long result) {
			
			FragmentManager fragmentManager = getFragmentManager();

			//Set the fragment animation
			FragmentTransaction transaction = fragmentManager.beginTransaction();
			transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);

			transaction.replace(R.id.container, showProductFragment);
			transaction.addToBackStack("showProductsSelector");
			transaction.commit();

	        ringProgressDialog.dismiss();
	     }

		
	 }

}