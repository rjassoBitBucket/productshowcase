package com.brighterbrain.productshowcase.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.brighterbrain.productshowcase.R;
import com.brighterbrain.productshowcase.utils.ProductUtils;

/**This fragment shows the product photo in full size.
 *  @author Rafael Jasso
 */
public class ShowProductPhotoFragment extends Fragment {
	View showProductsPhotoView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		//Load GUI
		showProductsPhotoView = inflater.inflate(R.layout.show_product_photo_fragment, container, false);
		
		//Load image container
		ImageView productPhotoImageView = (ImageView)showProductsPhotoView.findViewById(R.id.productPhotoFullSizeImageView);
				
		//Utils to handle product data
		ProductUtils utils = new ProductUtils(getActivity());
		
		//Load the encodedImage from the product
		Bundle args = getArguments();
		String encodedImage = args.getString("encodedImage");
		
		//Converts to bitmap and set the photo to the ImageView
		productPhotoImageView.setImageBitmap(utils.getImageFromEncodedString(encodedImage));
		
		return showProductsPhotoView;
	}
	
	//Release memory
	@Override
	public void onDestroy() {
		super.onDestroy();
		if(showProductsPhotoView!=null)
			showProductsPhotoView = null;
	}
}