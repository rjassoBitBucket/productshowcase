package com.brighterbrain.productshowcase.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.brighterbrain.productshowcase.R;
import com.brighterbrain.productshowcase.beans.Product;
import com.brighterbrain.productshowcase.utils.DBManager;
import com.brighterbrain.productshowcase.utils.ProductUtils;

/**
 * This class allow the user to insert 5 different products
 * from the hardcoded JSON (products_json) to a SQLite data base. 
 * @author Rafael Jasso
 *
 */
public class CreateProductsSelectorFragment extends Fragment {

	Button createProduct1Button;
	Button createProduct2Button;
	Button createProduct3Button;
	Button createProduct4Button;
	Button createProduct5Button;
	
	private DBManager dbConnection;
	private ProductUtils utils;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		//Data base connection variables
		dbConnection = new DBManager(getActivity());
		dbConnection.openDatabase();
		
		//Utilities for managing products data
		utils = new ProductUtils(getActivity());
		
		//Inflate layout and Buttons to handle on click events
		View createProductsView = inflater.inflate(R.layout.create_products_fragment, container, false);
		createProduct1Button = (Button)createProductsView.findViewById(R.id.createProduct1Button);
		createProduct2Button = (Button)createProductsView.findViewById(R.id.createProduct2Button);
		createProduct3Button = (Button)createProductsView.findViewById(R.id.createProduct3Button);
		createProduct4Button = (Button)createProductsView.findViewById(R.id.createProduct4Button);
		createProduct5Button = (Button)createProductsView.findViewById(R.id.createProduct5Button);
		
		createProduct1Button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				final Product product = utils.loadProductFromJSON(1);				
		    	dbConnection.addRow(product);
		    	Toast.makeText(getActivity(), product.getName() + " inserted successfully from JSON!", Toast.LENGTH_SHORT).show();
		    	
			}
		});
		createProduct2Button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final Product product = utils.loadProductFromJSON(2);				
		    	dbConnection.addRow(product);
		    	Toast.makeText(getActivity(), product.getName() + " inserted successfully from JSON!", Toast.LENGTH_SHORT).show();
			}
		});
		createProduct3Button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				final Product product = utils.loadProductFromJSON(3);				
		    	dbConnection.addRow(product);
		    	Toast.makeText(getActivity(), product.getName() + " inserted successfully from JSON!", Toast.LENGTH_SHORT).show();
			}
		});
		createProduct4Button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				final Product product = utils.loadProductFromJSON(4);				
		    	dbConnection.addRow(product);
		    	Toast.makeText(getActivity(), product.getName() + " inserted successfully from JSON!", Toast.LENGTH_SHORT).show();
			}
		});
		createProduct5Button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				final Product product = utils.loadProductFromJSON(5);				
		    	dbConnection.addRow(product);
		    	Toast.makeText(getActivity(), product.getName() + " inserted successfully from JSON!", Toast.LENGTH_SHORT).show();
			}
		});

		return createProductsView;
	}
	
	//Realize memory
	@Override
	public void onDestroy() {
		super.onDestroy();
		dbConnection.closeDatabase();
	}
	
	
}