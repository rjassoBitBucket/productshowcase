package com.brighterbrain.productshowcase.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.brighterbrain.productshowcase.R;

/**
 * This fragment is loaded when the app is started
 * Displays a menu to show or insert products
 * @author Rafael Jasso
 */
public class MainFragment extends Fragment {
	private Button createProductsButton;
	private Button showProductsButton;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		//inicialize GUI and buttons
		View mainView = inflater.inflate(R.layout.main_fragment, container, false);
		createProductsButton = (Button)mainView.findViewById(R.id.createProductsButton);
		showProductsButton = (Button)mainView.findViewById(R.id.showProductsButton);
		
		//Handle button's onclick events
		createProductsButton.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {

				FragmentManager fragmentManager = getFragmentManager();

				//Set the fragment animation
				FragmentTransaction transaction = fragmentManager.beginTransaction();
				transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);

				transaction.replace(R.id.container, new CreateProductsSelectorFragment());
				transaction.addToBackStack("createProductsSelector");
				transaction.commit();
			}
		});
		showProductsButton.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				
				FragmentManager fragmentManager = getFragmentManager();

				//Set the fragment animation
				FragmentTransaction transaction = fragmentManager.beginTransaction();
				transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);

				transaction.replace(R.id.container, new ShowProductSelectorFragment());
				transaction.addToBackStack("showProductSelectorFragment");
				transaction.commit();

			}
		});
		
		return mainView;
	}		
}