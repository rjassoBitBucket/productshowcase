package com.brighterbrain.productshowcase.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brighterbrain.productshowcase.R;
import com.brighterbrain.productshowcase.beans.Product;
import com.brighterbrain.productshowcase.utils.DBManager;
import com.brighterbrain.productshowcase.utils.ProductUtils;

/**
 * This fragment shows a product loaded from the database
 * @author Rafael Jasso
 */
public class ShowProductFragment extends Fragment {

	private View showProductsSelectorView;
	private DBManager dbConnection;
	private Bitmap productBitmapRealSize;
	private Bitmap productBitmapScaled;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		//Variables to make data base connections
		dbConnection = new DBManager(getActivity());
		dbConnection.openDatabase();
		
		//Inflate the GUI
		showProductsSelectorView = inflater.inflate(R.layout.show_product_fragment, container, false);
		
		//Inicialize GUI views
		TextView productNameTextView    = (TextView) showProductsSelectorView.findViewById(R.id.productNameTextView);
		ImageView productPhotoImageView = (ImageView)showProductsSelectorView.findViewById(R.id.productPhotoImageView);
		TextView productDescriptionTextView = (TextView) showProductsSelectorView.findViewById(R.id.productDescriptionTextView);
		TextView priceTextView = (TextView) showProductsSelectorView.findViewById(R.id.priceTextView);
		Button updateProductButton = (Button) showProductsSelectorView.findViewById(R.id.updateProductButton);
		Button deleteProductButton = (Button) showProductsSelectorView.findViewById(R.id.deleteProductButton);
		
		//Utils to manage products
		ProductUtils utils = new ProductUtils(getActivity());
		
		//Arguments to load the selected product
		Bundle args = getArguments();
		int productNumber = (args.getInt("productIndex"));
		final Product product = utils.loadProductFromDB(productNumber);
		
		//Scale the bitmap to save memory 
		productBitmapRealSize = utils.getImageFromEncodedString(product.getProductPhoto());
		productBitmapScaled = utils.decodeBitmap(productBitmapRealSize);
				
		//Set UI with the product data
		productNameTextView.setText(product.getName());
		productPhotoImageView.setImageBitmap(productBitmapScaled);
		productDescriptionTextView.setText(product.getDescription());
		priceTextView.setText(getString(R.string.dollar)+product.getSalePrice());
		
		//On click event handling
		productPhotoImageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Bundle args = new Bundle();
     		    args.putString("encodedImage", product.getProductPhoto());
			    ShowProductPhotoFragment showProductPhotoFragment = new ShowProductPhotoFragment();
			    showProductPhotoFragment.setArguments(args);
			    
			    FragmentManager fragmentManager = getFragmentManager();

				//Set the fragment animation
				FragmentTransaction transaction = fragmentManager.beginTransaction();
				transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);

				transaction.replace(R.id.container, showProductPhotoFragment);
				transaction.addToBackStack("showProductPhotoFragment");
				transaction.commit();
				
			}
		});
		
		updateProductButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
			}
		});
		deleteProductButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
		 
					// set title
					alertDialogBuilder.setTitle("Are you sure?");
		 
					// set dialog message
					alertDialogBuilder
						.setMessage("Do you want to delete the product " + product.getName() + "!")
						.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								// if this button is clicked, delete the current product
								dbConnection.deleteRow((long)product.getId());
								getActivity().getFragmentManager().popBackStack();
							}
						  })
						.setNegativeButton("No",new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								// just cancel the dialog
								dialog.cancel();
							}
						});
		 
						// create alert dialog
						AlertDialog alertDialog = alertDialogBuilder.create();
		 
						// show it
						alertDialog.show();
			}
		});
		
		return showProductsSelectorView;
	}
	
	//Release memory
	@Override
	public void onDestroy() {
		super.onDestroy();
		
		if(showProductsSelectorView!=null)
			showProductsSelectorView = null;
		
		//Release more memory
		if(productBitmapRealSize!=null)
			productBitmapRealSize.recycle();
		if(productBitmapScaled!=null)
			productBitmapScaled.recycle();
	}
}