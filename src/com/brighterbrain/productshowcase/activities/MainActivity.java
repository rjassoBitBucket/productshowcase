package com.brighterbrain.productshowcase.activities;

import android.app.Activity;
import android.os.Bundle;

import com.brighterbrain.productshowcase.R;
import com.brighterbrain.productshowcase.fragments.MainFragment;
/**
 * MainActivity Class, the fist activity.
 * @author Rafael Jasso
 *
 */
public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);
		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction().add(R.id.container, new MainFragment()).commit();
		}
	}
}
