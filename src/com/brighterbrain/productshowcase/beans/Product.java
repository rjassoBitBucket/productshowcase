package com.brighterbrain.productshowcase.beans;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Product bean. As the suggested data model.
 * @author Rafael Jasso
 *
 */
public class Product {
	private int id;
	private String name;
	private String description;
	private float regularPrice;
	private float salePrice;
	private String productPhoto;
	private String[] colors;
	private ArrayList<HashMap<String, String>> stores;
	
	public String[] getColors() {
		return colors;
	}
	public void setColors(String[] colors) {
		this.colors = colors;
	}
	public ArrayList<HashMap<String, String>> getStores() {
		return stores;
	}
	public void setStores(ArrayList<HashMap<String, String>> stores) {
		this.stores = stores;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public float getRegularPrice() {
		return regularPrice;
	}
	public void setRegularPrice(float regularPrice) {
		this.regularPrice = regularPrice;
	}
	public float getSalePrice() {
		return salePrice;
	}
	public void setSalePrice(float salePrice) {
		this.salePrice = salePrice;
	}
	public String getProductPhoto() {
		return productPhoto;
	}
	public void setProductPhoto(String productPhoto) {
		this.productPhoto = productPhoto;
	}
	
}
