package com.brighterbrain.productshowcase.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.brighterbrain.productshowcase.beans.Product;
/**
 * This class id a database helper.
 * Assist you to make the basic data base functions
 * SELECT, DELETE, UPDATE and INSERT.
 *  @author Rafael Jasso
 *
 */
public class DBManager {
	//Constants to manage data
	public static final String KEY_ID = "product_id";
    private static final String TAG = "DBManager";
    private static final String DATABASE_NAME = "ProductShowCase";
    private static final String DATABASE_TABLE = "Product";
    private static final int DATABASE_VERSION = 1;

    //Create table sql sentence
    private static final String DATABASE_CREATE = "create table " + DATABASE_TABLE + " ("+KEY_ID+" integer primary key, name TEXT, description TEXT, regular_price NUM, sale_price NUM, product_photo TEXT, colors_id INT, stores_id INT);";
        
    private final Context context;     
    private DatabaseHelper dbHelper;
    private SQLiteDatabase db;

    public DBManager(Context context)
    {
        this.context = context;
        dbHelper = new DatabaseHelper(this.context);
    }
        
    private static class DatabaseHelper extends SQLiteOpenHelper 
    {
        DatabaseHelper(Context context) 
        {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) 
        {
            db.execSQL(DATABASE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, 
        int newVersion) 
        {
            Log.w(TAG, "Upgrading database from version " + oldVersion 
                    + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS titles");
            onCreate(db);
        }
    }    
    
    public DBManager openDatabase() throws SQLException
    {
        db = dbHelper.getWritableDatabase();
        return this;
    }

    public void closeDatabase()
    {
        dbHelper.close();
    }
    
    /**
     * Here the products are inserted
     * @param product
     * @return
     */
    public long addRow(Product product)
    {
    	
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_ID, product.getId());
        initialValues.put("name", product.getName());
        initialValues.put("description", product.getDescription());
        initialValues.put("regular_price", product.getRegularPrice());
        initialValues.put("sale_price", product.getSalePrice());
        initialValues.put("product_photo", product.getProductPhoto());
        initialValues.put("colors_id", product.getId());
        initialValues.put("stores_id", product.getId());
        
		return db.insertWithOnConflict(DATABASE_TABLE, null, initialValues, SQLiteDatabase.CONFLICT_REPLACE);
    }

    /**
     * This method deletes the row with the given id
     * @param rowId
     * @return
     */
    public boolean deleteRow(long rowId)
    {
        return db.delete(DATABASE_TABLE, KEY_ID +"=" + rowId, null) > 0;
    }

    /**
     * This method returns the entire table 
     * @return
     */
    public Cursor getRows()
    {
        return db.query(DATABASE_TABLE, new String[] {KEY_ID, "name", "description", "sale_price", "product_photo"},
                null, 
                null, 
                null, 
                null, 
                null);
    }

    /**
     * This method returns a row with the given id
     * @param rowId
     * @return
     * @throws SQLException
     */
    public Cursor getRow(long rowId) throws SQLException
    {
        Cursor mCursor =
                db.query(true, DATABASE_TABLE, new String[] {KEY_ID, "name", "description", "sale_price", "product_photo"}, 
                		KEY_ID + "=" + rowId,
                		null,
                		null, 
                		null, 
                		null, 
                		null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    /**
     * This method updates the selected product with the given id
     * @param rowId
     * @param product
     * @return
     */
    public boolean updateRow(long rowId, Product product)
    {
        ContentValues args = new ContentValues();
        args.put(KEY_ID, rowId);
        args.put("name", product.getName());
        args.put("description", product.getDescription());
        args.put("regular_price", product.getRegularPrice());
        args.put("sale_price", product.getSalePrice());
        args.put("product_photo", product.getProductPhoto());
        args.put("colors_id", product.getId());
        args.put("stores_id", product.getId());
        return db.update(DATABASE_TABLE, args, KEY_ID + "=" + rowId, null) > 0;
    }
}
