package com.brighterbrain.productshowcase.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import com.brighterbrain.productshowcase.R;
import com.brighterbrain.productshowcase.beans.Product;

/**
 * This class makes easier to handle products
 *  @author Rafael Jasso
 *
 */
public class ProductUtils {
	Context context;
	
	/**
	 * Load context for future operations
	 * @param context
	 */
	public ProductUtils(Context context){
		this.context = context;
	}
	
	/**
	 * This method creates a specific Product object from JSON data
	 * @param productNumber
	 * @return
	 */
	public Product loadProductFromJSON(int productNumber){
		JSONObject productsJSON;
		JSONArray productsArray;
		JSONObject productJSON;
		Product product = new Product();
		
		try {
			String jsonString = context.getString(R.string.products_json);
			productsJSON = new JSONObject(jsonString);
			productsArray = productsJSON.getJSONArray("Products");
			productJSON = (JSONObject) productsArray.get(productNumber-1);
			
			product.setId(productJSON.getInt("id"));
			product.setName(productJSON.getString("name"));
			product.setDescription(productJSON.getString("description"));
			product.setRegularPrice((float)productJSON.getDouble("regularPrice"));
			product.setSalePrice((float)productJSON.getDouble("salePrice"));
			product.setProductPhoto(productJSON.getString("productPhoto"));
			product.setColors(jsonArrayToArray(jsonString, productNumber));			
			product.setStores(loadStoresDictionary(jsonString, productNumber));
			
		} catch (JSONException e) {
			Log.e("BrighterBrain", "Error parsing JSON: " + e.getMessage());
		}
		
		return product;
	}
	
	/**
	 *  This method creates a specific Product object from the data base
	 * @param productNumber
	 * @return
	 */
	public Product loadProductFromDB(int productNumber){
		Cursor dbCursor;
		DBManager dbConnection = new DBManager(context);
		Product product = new Product();
		
		dbConnection.openDatabase();
		dbCursor = dbConnection.getRow(productNumber);
		if(dbCursor.moveToFirst()){
			product.setId(dbCursor.getInt(0));
			product.setName(dbCursor.getString(1));
			product.setDescription(dbCursor.getString(2));
			product.setSalePrice((float) dbCursor.getDouble(3));
			product.setProductPhoto(dbCursor.getString(4));
		}
		
		return product;
	}

	/**
	 * This method generates the stores dictionary from JSON data from the selected product
	 * @param jsonString
	 * @param productNumber
	 * @return
	 * @throws JSONException
	 */
	public ArrayList<HashMap<String, String>> loadStoresDictionary(String jsonString, int productNumber) throws JSONException {
		JSONObject productsJSON = new JSONObject(jsonString);
		JSONArray productsJSONArray = productsJSON.getJSONArray("Products");
		JSONObject oneProductJSON = productsJSONArray.getJSONObject(productNumber-1);
		JSONArray storesJSONArray = oneProductJSON.getJSONArray("stores");
		ArrayList<HashMap<String, String>> stores = new ArrayList<HashMap<String, String>>();
		HashMap<String,String> store = new HashMap<String,String>();
		
		for(int i=0;i<storesJSONArray.length();i++){
			JSONObject storeJSON = storesJSONArray.getJSONObject(i);
			store.put("name", storeJSON.getString("name"));
			store.put("address",storeJSON.getString("address"));
			store.put("address",storeJSON.getString("phoneNumber"));
			stores.add(store);
		}
		return stores;
	}

	/**
	 * This method creates an array of strings from JSON data with the selected product.
	 * @param jsonString
	 * @param productNumber
	 * @return
	 */
	public String[] jsonArrayToArray(String jsonString, int productNumber){
		JSONObject jsonObject, productJSON;
		JSONArray jsonArray, productColorsArray;
		String[] colorsArray = null;
		try {
			jsonObject = new JSONObject(jsonString);
			jsonArray = jsonObject.getJSONArray("Products");
			productJSON = jsonArray.getJSONObject(productNumber-1);
			productColorsArray = productJSON.getJSONArray("colors");
			
			colorsArray = new String[productColorsArray.length()];
			
			for (int i = 0; i < productColorsArray.length(); i++) {
				colorsArray[i] = productColorsArray.getString(i);
			}
		} catch (JSONException e) {
			Log.e("BrighterBrain", "Error parsing JSON: " + e.getMessage());
		}
		return colorsArray;
	}
	
	/**
	 * This method converts an encoded base64 string to its bitmap representation
	 * @param encodedImage
	 * @return
	 */
	public Bitmap getImageFromEncodedString(String encodedImage){
		byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
		Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
		return decodedByte;
	}
	
	/**
	 * This method converts a array of string to an unique string.
	 * @param stringsArray
	 * @return
	 */
	public String arrayToString(String[] stringsArray){
		String string = "";
		for(int i = 0;i<stringsArray.length;i++){
			if(string.equals(""))
				string+=stringsArray[i];
			else
				string+=", " + stringsArray[i];
		}
		return string;
	}
	
	/**
	 * This method scale an image for reduce memory consumption.
	 * @param bitmap
	 * @return
	 */
	public Bitmap decodeBitmap(Bitmap bitmap){
	    try {
	        BitmapFactory.Options options = new BitmapFactory.Options();
	        options.inJustDecodeBounds = true;

	    	ByteArrayOutputStream bos = new ByteArrayOutputStream(); 
	    	bitmap.compress(CompressFormat.PNG, 0 /*ignored for PNG*/, bos); 
	    	byte[] bitmapdata = bos.toByteArray();
	    	ByteArrayInputStream bs = new ByteArrayInputStream(bitmapdata);
	    	
	        int scale=2;
	        BitmapFactory.Options options2 = new BitmapFactory.Options();
	        options2.inSampleSize=scale;
	        
	        return BitmapFactory.decodeStream(bs,null,options2);
	    } catch (Exception e) {}
	    return null;
	}   
	
}
